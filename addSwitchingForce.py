import numpy as np

def  insertIN(i, n):

    if n >= 1:
        iaux = i[n]
        j = n - 1
        while (i[j] > iaux) & (j >= 0):
            i[j + 1] = i[j]
            j = j - 1

        i[j + 1] = iaux



def addSwitchingForce(d1a, d2a, d3a, d4a, n, dSdr, dr,
        r1, dpole, qpole, opole, hpole):

    nM = np.shape(dpole)[1] 
    fsf = np.zeros((3, nM))

    in2 = np.zeros(2, dtype=int)
    in3 = np.zeros(3, dtype=int)
    in4 = np.zeros(4, dtype=int)

    for i in range(3):
        for j in range(3):
            in2[0] = i
            in2[1] = j
            insertIN(in2, 1)
          
            d2a[i, j] = d2a[in2[0], in2[1]]

            for k in range(3):
                for ii in range(2):
                    in3[ii] = in2[ii]
                in3[2] = k
                insertIN(in3, 2)

                d3a[i, j, k] = d3a[in3[0], in3[1], in3[2]]

                for l in range(3): 
                    for ii in range(3):
                        in4[ii] = in3[ii]
                    in4[3] = l
                    insertIN(in4, 3)

                    d4a[i, j, k, l] = d4a[in4[0], in4[1], in4[2], in4[3]]
                   
    
    u = 0.
    for i in range(3):
        u += d1a[i] * dpole[i, n]
        for j in range(3):
            u += d2a[i, j] * qpole[i, j, n] / 3.
            for k in range(3):
                u += d3a[i, j, k] * opole[i, j, k, n] / 15.
                for l in range(3):
                    u += d4a[i, j, k, l] * hpole[i, j, k, l, n] / 105.

    u *= - dSdr / r1
    for i in range(3):
        fsf[i, n] = fsf[i, n] + u * dr[i]

    return fsf

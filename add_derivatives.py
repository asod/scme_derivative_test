import numpy as np
def addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d, d3d, d4d, d5d):
#    d1t = np.zeros_like(d1a)
#    d2t = np.zeros_like(d2a)
#    d3t = np.zeros_like(d3a)
#    d4t = np.zeros_like(d4a)
#    d5t = np.zeros_like(d5a)
    for i in range(3):
        d1a[i] = d1a[i] + d1d[i]
        for j in range(i, 3):
            d2a[i, j] = d2a[i, j] + d2d[i, j]
            for k in range(j, 3):
                d3a[i, j, k] = d3a[i, j, k] + d3d[i, j, k]
                for l in range(k, 3):
                    d4a[i, j, k, l] = d4a[i, j, k, l] + d4d[i, j, k, l]
                    for s in range(l, 3):
                        d5a[i, j, k, l, s] = d5a[i, j, k, l, s] + d5d[i, j, k, l, s]

    return d1a, d2a, d3a, d4a, d5a
#    return d1t, d2t, d3t, d4t, d5t

def addDeriv(d1v, d2v, d3v, d4v, d5v, d1d, d2d, d3d, d4d, d5d, n, swFunc):
#    d1t = np.zeros_like(d1v)
#    d2t = np.zeros_like(d2v)
#    d3t = np.zeros_like(d3v)
#    d4t = np.zeros_like(d4v)
#    d5t = np.zeros_like(d5v)
    for i in range(3):
        d1v[i, n] = d1v[i, n] + d1d[i] * swFunc
        for j in range(i, 3):
            d2v[i, j, n] = d2v[i, j, n] + d2d[i, j] * swFunc
            for k in range(j, 3):
                d3v[i, j, k, n] = d3v[i, j, k, n] + d3d[i, j, k] * swFunc
                for l in range(k, 3):
                    d4v[i, j, k, l, n] = d4v[i, j, k, l, n] + d4d[i, j, k, l] * swFunc
                    for s in range(l, 3):
                        d5v[i, j, k, l, s, n] = d5v[i, j, k, l, s, n] + d5d[i, j, k, l, s] * swFunc
#    print 'd1t in addDeriv: {0}'.format(d1t)
    return d1v, d2v, d3v, d4v, d5v
    #return d1t, d2t, d3t, d4t, d5t

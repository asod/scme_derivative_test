import numpy as np

def delta(i, j):
    if i == j:
        delta = 1.
    else:
        delta = 0
    return delta

def dDpole(d, r):

    d1d = np.zeros(3)
    d2d = np.zeros((3, 3))
    d3d = np.zeros((3, 3, 3))
    d4d = np.zeros((3, 3, 3, 3))
    d5d = np.zeros((3, 3, 3, 3, 3))

    r2 = r[0]**2 + r[1]**2 + r[2]**2
    r3 = np.sqrt(r2) * r2
    r5 = r3 * r2
    r7 = r5 * r2
    r9 = r7 * r2
    r11 = r9 * r2
    r13 = r11 * r2

    r3  =        1.00 / r3
    r5  =       -3.00 / r5
    r7  =       15.00 / r7
    r9  =     -105.00 / r9
    r11 =      945.00 / r11
    r13 =   -10395.00 / r13

    rd = r[0]*d[0] + r[1]*d[1] + r[2]*d[2]

    for i in range(3):
        d1d[i] = d[i] * r3 + rd * r[i] * r5
        
        for j in range(3):
            dij = delta(i, j)

            # 2nd derivative
            t1 = d[i] * r[j] + d[j] * r[i] + rd * dij
            t2 = rd * r[i] * r[j]

            d2d[i, j] = t1 * r5 + t2 * r7

            for k in range(j, 3): 
                dik = delta(i, k)
                djk = delta(j, k)

                #     3rd derivative

                dt1k = d[i] * djk + d[j] * dik + d[k] * dij
                y1 = dt1k
                dt2k = d[k] * r[i] * r[j] + rd * (r[i] * djk + dik * r[j])
                y2 = t1 * r[k] + dt2k
                y3 = t2 * r[k]

                d3d[i, j, k] = y1 * r5 + y2 * r7 + y3 * r9

                for l in range(k, 3):
                    dil = delta(i ,l)
                    djl = delta(j, l)
                    dkl = delta(k, l)

                    #!  4th derivative

                    dt1l = d[i] * djl + d[j] * dil + d[l] * dij
                    ddt2kl = d[k] * (r[i] * djl + dil * r[j]) +\
                             d[l] * (r[i] * djk + dik * r[j]) +\
                             rd * (dil * djk + dik * djl)
                    dy2l = dt1l * r[k] + t1 * dkl + ddt2kl
                    z1 = y1 * r[l] + dy2l

                    dt2l = d[l] * r[i] * r[j] + rd * (r[i] * djl + dil * r[j])
                    dy3l = dt2l * r[k] + t2 * dkl
                    z2 = y2 * r[l] + dy3l

                    z3 = y3 * r[l]

                    d4d[i, j, k, l] =  z1 * r7 + z2 * r9 + z3 * r11

                    for s in range(l, 3):
                        dis = delta(i,s)
                        djs = delta(j,s)
                        dks = delta(k,s)
                        dls = delta(l,s)

                        #!     5th derivative
                        dddt2kls = d[k] * (dis * djl + dil * djs) +\
                                   d[l] * (dis * djk + dik * djs) +\
                                   d[s] * (dil * djk + dik * djl)
                        dt1s = d[i] * djs + d[j] * dis + d[s] * dij

                        ddy2ls = dt1l * dks + dt1s * dkl + dddt2kls
                        dz1s = y1 * dls + ddy2ls
                        w1 = dz1s


                        ddt2ks = d[k] * (r[i] * djs + dis * r[j]) +\
                                 d[s] * (r[i] * djk + dik * r[j]) +\
                                 rd * (dis * djk + dik * djs)
                        dy2s = dt1s * r[k] + t1 * dks + ddt2ks

                        ddt2ls = d[l] * (dis * r[j] + r[i] * djs) +\
                                 d[s] * (r[i] * djl + dil * r[j]) +\
                                 rd * (dis * djl + dil *djs)
                        dt2s = d[s] * r[i] * r[j] + rd *(r[i]*djs + dis * r[j])
                        ddy3ls = ddt2ls * r[k] + dt2l * dks + dt2s * dkl

                        dz2s = dy2s * r[l] + y2 * dls + ddy3ls
                        w2 = z1 * r[s] + dz2s

                        dy3s = dt2s * r[k] + t2 * dks
                        dz3s = dy3s * r[l] + y3 * dls
                        w3 = z2 * r[s] + dz3s

                        w4 = z3 * r[s]

                        d5d[i, j, k, l, s] = w1 * r7 +\
                                             w2 * r9 +\
                                             w3 * r11 +\
                                             w4 * r13


    return d1d, d2d, d3d, d4d, d5d


def dQpole(q, r):

    d1q = np.zeros(3)
    d2q = np.zeros((3, 3))
    d3q = np.zeros((3, 3, 3))
    d4q = np.zeros((3, 3, 3, 3))
    d5q = np.zeros((3, 3, 3, 3, 3))

    r2 = r[0]**2 + r[1]**2 + r[2]**2
    r3 = np.sqrt(r2) * r2
    r5 = r3 * r2
    r7 = r5 * r2
    r9 = r7 * r2
    r11 = r9 * r2
    r13 = r11 * r2
    r15 = r13 * r2

    r5  =        1.0 / r5
    r7  =       -5.0 / r7
    r9  =       35.0 / r9
    r11 =     -315.0 / r11
    r13 =     3465.0 / r13
    r15 =   -45045.0 / r15

    rrq = 0.0
    v = np.zeros(3)
    for j in range(3):
        for i in range(3):
            rrq += q[i, j] * r[i] * r[j]
            v[j] += q[i, j] * r[i]

    for i in range(3):
        d1q[i] = (2. * v[i]) * r5 +(rrq * r[i]) *r7

        for j in range(i, 3):
            dij = delta(i, j)

            #   2nd derivative
            t1 = 2. * q[i, j]
            t2 = 2. * (v[i] * r[j] + v[j] * r[i]) + rrq * dij
            t3 = rrq * r[i] * r[j]

            d2q[i ,j] = t1 * r5 + t2 * r7 + t3 * r9
            # print 'd2q[{0}, {1}]: {2}'.format(i, j, d2q[i, j]) #XXX CHECKED
            #print 'd2q iside dQpole loop: {0}'.format(d2q)

            for k in range(j, 3):
                dik = delta(i, k)
                djk = delta(j, k)
               
                # 3rd derivative
                dt2k = 2. * (v[i] * djk + v[j] * dik + v[k] * dij) + 2. *\
                            (q[i, k] * r[j] + q[j, k] * r[i])
 
                dt3k = 2. * v[k] * r[i] * r[j] + rrq *\
                           (r[i] * djk + r[j] * dik)
 
                y1 = t1*r[k] + dt2k
                y2 = t2*r[k] + dt3k
                y3 = t3*r[k]
               
                d3q[i, j, k] = y1 * r7 + y2 * r9 + y3 * r11

                for l in range(k, 3):
                    dil = delta(i, l)
                    djl = delta(j, l)
                    dkl = delta(k, l)
               
                    # 4th derivative
                    ddt2kl = 2. * (q[i, l] * djk + q[j, l] *
                                  dik + q[k, l] * dij) + 2. *\
                                  (q[i,k]*djl + q[j,k]*dil)

                    dy1l = t1 * dkl + ddt2kl

                    z1 = dy1l

                    dt2l = 2. * (v[i] * djl + v[j] * dil + v[l] * dij) +\
                           2. * (q[i, l] * r[j] + q[j, l] * r[i])

                    ddt3kl = 2. * q[k, l] * r[i] * r[j] +\
                             2. * v[k] * (r[i] * djl + dil * r[j]) +\
                             2. * v[l] * (r[i] * djk + r[j] * dik) +\
                             rrq * (dil*djk + djl*dik)
               
                    dy2l = dt2l * r[k] + t2 * dkl + ddt3kl

                    z2 = y1 * r[l] + dy2l
                  
                    dt3l = 2. * v[l] * r[i] * r[j] + rrq *\
                               (r[i] * djl + r[j] * dil)

                    dy3l = dt3l * r[k] + t3 * dkl
                    z3 = y2 * r[l] + dy3l

                    z4 = y3 * r[l]
                    d4q[i,j,k,l] =  z1 * r7 + z2 * r9 + z3 * r11 + z4 * r13
                  
                    for s in range(l, 3):
                        dis = delta(i, s)
                        djs = delta(j, s)
                        dks = delta(k, s)
                        dls = delta(l, s)


                        # 5th derivative
                        dddt3kls = 2. * q[k, l] * (r[i] * djs + dis * r[j]) +\
                                   2. * q[k, s] * (r[i] * djl + dil * r[j]) +\
                                   2. * v[k] * (dis * djl + dil * djs) +\
                                   2. * q[l,s] * (r[i] * djk + r[j] * dik) +\
                                   2. * v[l] * (dis * djk + djs * dik) +\
                                   2. * v[s] * (dil * djk + djl * dik)

                        dt2s = 2. * (v[i] * djs + v[j] * dis + v[s] * dij) +\
                               2. * (q[i, s] * r[j] + q[j, s] * r[i])

                        ddt2ls = 2. * (q[i, s] * djl + q[j, s] * dil + q[l, s]
                                 * dij) + 2. * (q[i, l] * djs + q[j, l] * dis)

                        ddy2ls = ddt2ls * r[k] + dt2l * dks + dt2s*dkl +\
                                 dddt3kls  

                        ddt2ks = 2. * (q[i, s] * djk + q[j, s] * dik + q[k, s]
                                 * dij) + 2. * (q[i, k] * djs + q[j, k] * dis)

                        dy1s = t1 * dks + ddt2ks

                        dz2s = y1 * dls + dy1s * r[l] + ddy2ls
                        w1 = z1 * r[s] + dz2s

                        ddt3ks = 2. * q[k, s] * r[i] * r[j] + 2. * v[k] *\
                                 (r[i] * djs + dis * r[j] ) +\
                                 2. * v[s] * (r[i] * djk + r[j] * dik) +\
                                 rrq * (dis * djk + djs * dik)

                        dy2s = dt2s * r[k] + t2 * dks + ddt3ks

                     
                        ddt3ls = 2. * (q[l, s] * r[i] * r[j] + v[l] *
                                 (dis * r[j] + r[i]*djs) ) +\
                                 2. * v[s] * (r[i] * djl + r[j] * dil) +\
                                 rrq * (dis * djl + djs * dil)

                        dt3s = 2. * v[s] * r[i] * r[j] + rrq * (r[i]*djs +
                               r[j]*dis)

                        ddy3ls = ddt3ls * r[k] + dt3l * dks + dt3s * dkl

                        dz3s = dy2s * r[l] + y2 * dls + ddy3ls
                        w2 = z2 * r[s] + dz3s

                        dy3s = dt3s * r[k] + t3 * dks

                        dz4s = dy3s * r[l] + y3 * dls
                        w3 = z3 * r[s] + dz4s

                        w4 = z4 * r[s] 
                     
                        d5q[i, j, k, l, s] = w1 * r9 + w2 * r11 +\
                                             w3 * r13 + w4 * r15
                     
    #print 'd2q in dQpole: {0}'.format(d2q)
    return d1q, d2q, d3q, d4q, d5q

def dOpole(o, r):
       
    d1o = np.zeros(3)
    d2o = np.zeros((3, 3))
    d3o = np.zeros((3, 3, 3))
    d4o = np.zeros((3, 3, 3, 3))
    d5o = np.zeros((3, 3, 3, 3, 3))
    g = np.zeros((3, 3))
    v = np.zeros(3)

    r2 = r[0]**2 + r[1]**2 + r[2]**2
    r3 = np.sqrt(r2) * r2
    r5 = r3 * r2
    r7 = r5 * r2
    r9 = r7 * r2
    r11 = r9 * r2
    r13 = r11 * r2
    r15 = r13 * r2
    r17 = r15 * r2

    r7  =       1. / r7
    r9  =      -7. / r9
    r11 =      63. / r11
    r13 =    -693. / r13
    r15 =    9009. / r15
    r17 = -135135. / r17

    r3o = 0.
    for k in range(3):
        for j in range(3):
            for i in range(3):
                r3o  = r3o + o[i, j, k] * r[i] * r[j] * r[k]
                v[k] = v[k] + o[i, j, k] * r[i] * r[j] 
                g[j, k] = g[j, k] + o[i, j, k] * r[i]

    for i in range(3):
        d1o[i] = (3. * v[i]) * r7 + (r3o * r[i]) * r9
        for j in range(i, 3):
            dij = delta(i,j)

            #  2nd derivative
            t1 = 6. * g[i, j]
            t2 = 3. * (v[i] * r[j] + v[j] * r[i]) + r3o * dij
            t3 = r3o * r[i] * r[j]

            d2o[i, j] = t1 * r7 + t2 * r9 + t3 * r11
            for k in range(j, 3):
                dik = delta(i,k)
                djk = delta(j,k)
             
                # 3rd derivative
                dt1k = 6. * o[i,j,k]
                dt2k = 3. * (v[i] * djk + v[j] * dik + v[k] * dij) +\
                       6. * (g[i, k] * r[j] + g[j, k] * r[i])
                dt3k = 3. * v[k] * r[i] * r[j] + r3o *\
                            (r[i] * djk + r[j] * dik)
                y1 = dt1k 
                y2 = t1 * r[k] + dt2k
                y3 = t2 * r[k] + dt3k
                y4 = t3 * r[k]
             
                d3o[i, j, k] = y1 * r7 + y2 * r9 + y3 * r11 + y4 * r13

                for l in range(k, 3):
                    dil = delta(i, l)
                    djl = delta(j, l)
                    dkl = delta(k, l)
             
                    # 4th derivative
                    dt1l = 6. * o[i, j, l]
                    ddt2kl = 6. * (g[i, l] * djk + g[j, l] * dik + g[k, l] * dij) +\
                             6. * (o[i, k, l] * r[j] + o[j, k, l] * r[i]) +\
                             6. * (g[i, k] * djl + g[j, k] * dil)
                    dy2l = dt1l * r[k] + t1 * dkl + ddt2kl
                    z1 = y1 * r[l] + dy2l

                    dt2l = 3. * (v[i] * djl + v[j] * dil + v[l] * dij) +\
                           6. * (g[i, l] * r[j] + g[j, l] * r[i])
                 
                    ddt3kl = 3. * (2. * g[k, l] * r[i] * r[j] +\
                             v[k] * (dil * r[j] + r[i] * djl)) +\
                             r3o * (dil * djk + djl * dik) +\
                             3. * v[l] * (r[i] * djk + r[j] * dik)

                    dy3l = dt2l * r[k] + t2 * dkl + ddt3kl
                    z2 = y2 * r[l] + dy3l
                    
                    dt3l = 3. * v[l] * r[i] * r[j] + r3o * (r[i] * djl + r[j] * dil)
                    dy4l = dt3l * r[k] + t3 * dkl
                    z3 = y3 * r[l] + dy4l

                    z4 = y4 * r[l]
                    d4o[i, j, k, l] =  z1 * r9 + z2 * r11 + z3 * r13 + z4 * r15
                    
                    for s in range(l, 3):
                        dis = delta(i, s)
                        djs = delta(j, s)
                        dks = delta(k, s)
                        dls = delta(l, s)


                        # 5th derivative
                        dt1s = 6. * o[i, j, s]
     
                        dddt2kls = 6. *\
                                  (o[i, l, s] * djk + o[j, l, s] * dik +\
                                   o[k, l, s] * dij + o[i, k, l] * djs +\
                                   o[j, k, l] * dis + o[i, k, s] * djl +\
                                   o[j, k, s] * dil)
     
                        ddy2ls = dt1l * dks + dt1s * dkl + dddt2kls
              
                        dz1s = y1 * dls + ddy2ls
                        w1 = dz1s
     
                        ddt2ks = 6. *\
                                (g[i, s] * djk + g[j, s] * dik + g[k, s] * dij) +\
                                 6. * (o[i, k, s] * r[j] + o[j, k, s] * r[i]) +\
                                 6. * (g[i, k] * djs + g[j, k] * dis)
                        dy2s = dt1s * r[k] + t1 * dks + ddt2ks
     
                        ddt2ls = 6. *\
                                (g[i, s] * djl + g[j, s] * dil + g[l,s] * dij) +\
                                 6. * (o[i, l, s] * r[j] + o[j, l, s] * r[i]) +\
                                 6. * (g[i, l] * djs + g[j, l] * dis)
     
                        dt2s = 3. * (v[i] * djs + v[j] * dis + v[s] * dij) +\
                               6. * (g[i, s] * r[j] + g[j, s] * r[i])
     
                        dddt3kls = 6. * (o[k, l, s] * r[i] * r[j] + g[k, l] *\
                                   (dis * r[j] + r[i] * djs)) +\
                                   6. * g[k, s] * (dil * r[j] + r[i] * djl) +\
                                   3. * v[k] * (dil * djs + dis * djl) +\
                                   3. * v[s] * (dil * djk + djl * dik) +\
                                   6. * g[l, s] * (r[i] * djk + r[j] * dik) +\
                                   3. * v[l] * (dis * djk + djs * dik)
         
                        ddy3ls = ddt2ls * r[k] + dt2l * dks + dt2s * dkl + dddt3kls
     
                        dz2s = dy2s * r[l] + y2 * dls + ddy3ls
     
                        w2 = z1 * r[s] + dz2s
     
                        ddt3ks = 6. * g[k, s] * r[i] * r[j] +\
                                 3. * v[k] * (dis * r[j] + r[i] * djs) +\
                                 3. * v[s] * (r[i] * djk + r[j] * dik) +\
                                 r3o * (dis * djk + djs * dik)
     
                        dy3s = dt2s * r[k] + t2 * dks + ddt3ks
     
                        dt3s = 3. * v[s] * r[i] * r[j] + r3o * (r[i] * djs + r[j]*dis)
     
                        ddt3ls = 6. * g[l, s] * r[i] * r[j] +\
                                 3. * v[l] * (dis * r[j] + r[i] * djs) +\
                                 3. * v[s] * (r[i] * djl + r[j] * dil) +\
                                 r3o * (dis * djl + djs * dil)
     
                        ddy4ls = ddt3ls * r[k] + dt3l * dks + dt3s * dkl
     
                        dz3s = dy3s * r[l] + y3 * dls + ddy4ls
                                          
                        w3 = z2 * r[s] + dz3s
     
                        dy4s = dt3s * r[k] + t3 * dks
                        dz4s = dy4s * r[l] + y4 * dls
     
                        w4 = z3 * r[s] + dz4s
     
                        w5 = z4 * r[s]
                        
                        d5o[i, j, k, l, s] = w1 * r9 + w2 * r11 +\
                                             w3 * r13 + w4 * r15 + w5 * r17


    return d1o, d2o, d3o, d4o, d5o


def dHpole(h, r):
    
    d1h = np.zeros(3)
    d2h = np.zeros((3, 3))
    d3h = np.zeros((3, 3, 3))
    d4h = np.zeros((3, 3, 3, 3))
    d5h = np.zeros((3, 3, 3, 3, 3))
    
    v = np.zeros(3)
    g = np.zeros((3, 3))
    d = np.zeros((3, 3, 3))

    r2 = r[0]**2 + r[1]**2 + r[2]**2
    r3 = np.sqrt(r2) * r2
    r5 = r3 * r2
    r7 = r5 * r2
    r9 = r7 * r2
    r11 = r9 * r2
    r13 = r11 * r2
    r15 = r13 * r2
    r17 = r15 * r2
    r19 = r17 * r2

    r9  =        1. / r9
    r11 =       -9. / r11
    r13 =       99. / r13
    r15 =    -1287. / r15
    r17 =    19305. / r17
    r19 =  -328185. / r19

    r4h = 0.
    for l in range(3):
        for k in range(3):
            for j in range(3):
                for i in range(3):
                    r4h +=  h[i, j, k, l] * r[i] * r[j] * r[k] * r[l]
                    v[l] += h[i, j, k, l] * r[i] * r[j] * r[k] 
                    g[l, k] += h[i, j, k, l] * r[i] * r[j]
                    d[l, k, j] += h[i, j, k, l] * r[i]

    for i in range(3):
        d1h[i] = (4. * v[i]) * r9 + (r4h * r[i]) * r11
        for j in range(i, 3):
            dij = delta(i, j)

            # 2nd derivative
            t1 = 12. * g[i, j]
            t2 = 4. * (v[i] * r[j] + v[j] * r[i]) + r4h * dij
            t3 = r4h * r[i] * r[j]

            d2h[i, j] = t1 * r9 + t2 * r11 + t3 * r13

            for k in range(j, 3):
                dik = delta(i, k)
                djk = delta(j, k)
             
                # 3rd derivative
                dt1k = 24. * d[i, j, k]
                dt2k = 4. * (v[i] * djk + v[j] * dik + v[k] * dij) +\
                       12. * (g[i, k] * r[j] + g[j, k] * r[i])
                dt3k = 4. * v[k] * r[i] * r[j] +\
                       r4h * (r[i]*djk + r[j]*dik)
                y1 = dt1k 
                y2 = t1 * r[k] + dt2k
                y3 = t2 * r[k] + dt3k
                y4 = t3 * r[k]
             
                d3h[i, j, k] = y1*r9 + y2*r11 + y3*r13 + y4*r15

                for l in range(k, 3):
                    dil = delta(i, l)
                    djl = delta(j, l)
                    dkl = delta(k, l)
                 
                    # 4th derivative
                    dy1l = 24. * h[i, j, k, l]
                    z1 = dy1l

                    dt1l = 24. * d[i, j, l]
                    ddt2kl = 12. * (g[i, l] * djk + g[j, l] * dik +
                                    g[k, l] * dij) +\
                             24. * (d[i, k, l] * r[j] + d[j, k, l] * r[i]) +\
                             12. * (g[i, k] * djl + g[j, k] * dil)
                    dy2l = dt1l * r[k] + t1 * dkl + ddt2kl
                    z2 = y1 * r[l] + dy2l

                    dt2l = 4. * (v[i] * djl + v[j] * dil + v[l] * dij) +\
                           12. * (g[i, l] * r[j] + g[j, l] * r[i])
                 
                    ddt3kl = 4. * (3. * g[k, l] * r[i] * r[j] +
                                   v[k] * (dil*r[j] + r[i]*djl)) +\
                             r4h * (dil*djk + djl*dik) +\
                             4. * v[l] * (r[i] * djk + r[j] * dik)

                    dy3l = dt2l * r[k] + t2 * dkl + ddt3kl
                    z3 = y2 * r[l] + dy3l
                    
                    dt3l = 4. * v[l] * r[i] * r[j] + r4h *\
                                (r[i] * djl + r[j] * dil)

                    dy4l = dt3l * r[k] + t3 * dkl
                    z4 = y3 * r[l] + dy4l
                    z5 = y4 * r[l]

                    d4h[i, j, k, l] = z1 * r9 + z2 * r11 + z3 * r13 +\
                                      z4 * r15 + z5 * r17 
                   
                    for s in range(l, 3):
                        dis = delta(i, s)
                        djs = delta(j, s)
                        dks = delta(k, s)
                        dls = delta(l, s)

                        # 5th derivative
                        ddt1ls = 24. * h[i, j, l, s]
                        dt1s = 24. * d[i, j, s]

                        dddt2kls = 24. * (d[i, l, s] * djk + d[j, l, s] * dik +
                                          d[k, l, s] * dij) +\
                                   24. * (h[i, k, l, s] * r[j] +
                                          h[j, k, l, s] * r[i] + 
                                          d[i, k, l] * djs + d[j, k, l] * dis) +\
                                   24. * (d[i, k, s] * djl + d[j, k, s] * dil) 

                       
                        ddy2ls = ddt1ls * r[k] + dt1l * dks + dt1s * dkl +\
                                 dddt2kls

                        dy1s = 24. * h[i, j, k, s]
                        dz2s = y1 * dls + dy1s * r[l] + ddy2ls
                        w1 = z1 * r[s] + dz2s

                        ddt2ks = 12. * (g[i, s] * djk + g[j, s] * dik + g[k, s]
                                        * dij) +\
                                 24. * (d[i, k, s] * r[j] + d[j, k, s] * r[i]) +\
                                 12. * (g[i, k] * djs + g[j, k] * dis)

                        dy2s = dt1s * r[k] + t1 * dks + ddt2ks

                        dt2s = 4. * (v[i] * djs + v[j] * dis + v[s] * dij) +\
                               12.* (g[i, s] * r[j] + g[j, s] * r[i])

                        ddt2ls = 12. * (g[i, s] * djl + g[j, s] * dil + g[l, s] * dij) +\
                                 24. * (d[i, l, s] * r[j] + d[j, l, s] * r[i]) +\
                                 12. * (g[i, l] * djs + g[j, l] * dis)

                        dddt3kls = 12. * (2. * d[k, l, s] * r[i] * r[j] +
                                          g[k, l] * (dis*r[j] + r[i]*djs)) +\
                                    4. * (3. * g[k, s] * (dil * r[j] + r[i] * djl) +
                                          v[k] * (dil * djs + dis * djl)) +\
                                    4. * v[s]  * (dil * djk + djl * dik) +\
                                    4. * (v[l] * (dis * djk + djs * dik) +
                                    3. * g[l, s] * (r[i] * djk + r[j] * dik))


                        ddy3ls = ddt2ls * r[k] + dt2l * dks + dt2s * dkl +\
                                 dddt3kls

                        dz3s = dy2s * r[l] + y2 * dls + ddy3ls

                        w2 = z2 * r[s] + dz3s

                        ddt3ks = 4. * (3. * g[k, s]*r[i]*r[j] +
                                       v[k] * (dis * r[j] + r[i] * djs)) +\
                                 r4h * (dis*djk + djs*dik) +\
                                 4. * v[s] * (r[i] * djk + r[j] * dik)

                        dy3s = dt2s * r[k] + t2 * dks + ddt3ks

                        dt3s = 4. * v[s] * r[i] * r[j] + r4h * (r[i] * djs +
                                                                r[j] * dis)

                        ddt3ls = 4. * (3. * g[l, s] * r[i] * r[j] +
                                       v[l] * (dis * r[j] + r[i] * djs)) +\
                                 r4h * (dis * djl + djs * dil) +\
                                 4. * v[s] * (r[i] * djl + r[j] * dil)

                        ddy4ls = ddt3ls * r[k] + dt3l * dks + dt3s * dkl

                        dz4s = dy3s * r[l] + y3 * dls + ddy4ls

                        w3 = z3 * r[s] + dz4s

                        dy4s = dt3s * r[k] + t3 * dks
                        dz5s = dy4s * r[l] + y4 * dls

                        w4 = z4 * r[s] + dz5s 

                        w5 = z5 * r[s]

                        d5h[i, j, k, l, s] = w1 * r11 + w2 * r13 + w3 * r15 +\
                                             w4 * r17 + w5 * r19
                        

    return d1h,  d2h,  d3h,  d4h,  d5h

import numpy as np
from dpoles import dDpole, dQpole, dOpole, dHpole
from addSwitchingForce import addSwitchingForce, insertIN
from add_derivatives import addDeriv, addDerivA

def tang_toenniesN(r, n):
    b = 4.4
    x = b * r
    f = 1
    t = np.exp(-x)
    for k in range(n + 1):
        f = f - t
        t = t * x / (k + 1)
    f = np.sqrt(f)
    return f

def tang_toenniesNdF(r, n):
    b = 4.4
    x = b * r
    f = 1.0
    df = 0.0
    t = np.exp(-x)
    for k in range(n + 1):
        f = f - t
        df = df - t * b * (-1.0 + k / x)
        t = t * x / (k + 1)

    f = np.sqrt(f)
    df = df / (2.0 * f)

    return f, df


def SF(r):
    rL1 = 0.
    rH1 = 5. 
    rL2 = 9.
    rH2 = 11.
    
    if (r >= rH2) | (r <= rL1):
        swFunc = 0.0
    elif (r >= rH1) & (r <= rL2):
        swFunc = 1.0
    elif (r < rH1):
        swFunc = tang_toenniesN(r, 6)
    else:
        x = (r - rL2)/(rH2 - rL2)
        x2 = x**2
        x3 = x2 * x
        swFunc = 1.0 + x3 * (-6.0 * x2 + 15.0 * x - 10.0)

    return swFunc

def SFdsf(r):
    rL1 = 0.
    rH1 = 5. 
    rL2 = 9.
    rH2 = 11.
    if (r >= rH2) | (r <= rL1):
        swFunc = 0.0
        dSdr = 0.0
    elif (r >= rH1) & (r <= rL2):
        swFunc = 1.0
        dSdr = 0
    elif  (r < rH1):
        swFunc, dSdr = tang_toenniesNdF(r, 6)
    else:
        x = (r - rL2)/(rH2 - rL2)
        dr = 1.0 / (rH2 - rL2)
        x2 = x * x
        x3 = x2 * x
        swFunc = 1.0 + x3 * (-6.0 * x2 + 15.0 * x - 10.0)
        dSdr = 30.0 * x2 * (- x2 + 2.0 * x - 1.0) * dr

    return swFunc, dSdr
    

def dField(dr, r2, r3, r5, dpole, m):
    eD = np.zeros(3)
    dEdr = np.zeros((3, 3))
    mDr = dpole[0, m] * dr[0] + dpole[1, m] * dr[1] + dpole[2, m] * dr[2]
    #print mDr
    for i in range(3):
        eD[i] = (3.0 * mDr * dr[i] / r2 - dpole[i ,m]) / r3
        for j in range(i, 3):
            dEdr[i ,j] = (dpole[i, m] * dr[j] + dpole[j, m] * dr[i] - 5. *
                          mDr * dr[i] * dr[j] / r2) * 3.0 / r5
            if i == j:
                dEdr[i, j] = dEdr[i, j] + mDr * 3.0 / r5

    u = mDr / r3
    return u, eD, dEdr

def qField(dr, r2, r5, r7, qpole, m):
    eq = np.zeros(3)
    dEdr = np.zeros((3, 3))
    v = np.zeros(3)
    rQr = 0.0
    for j in range(3):
        for i in range(3):
            v[i] = v[i] + qpole[i, j, m] * dr[j]
            rQr += dr[i] * qpole[i, j, m] * dr[j]
    for i in range(3):
        eq[i] = 2. * (2.5 * rQr / r2 * dr[i] - v[i]) / r5
        for j in range(i, 3):
            dEdr[i, j] = (-2 * qpole[i, j, m] * r2 + 10 * (v[j] *
                         dr[i] + v[i] * dr[j]) - 35 * rQr * dr[i] * dr[j] / r2) / r7
            if i == j:
                dEdr[i, j] = dEdr[i, j] + 5.00 * rQr / r7

    u = rQr / r5

    return u, eq, dEdr


def calcEdip_quad(atoms, dpole, qpole):
    """ Field from Dipole and quadropole. """
    # outputs: uD, uQ, eT, dEdr, 
    uD = 0.
    uQ = 0.
    dr = np.zeros(3)
    rMax2 = np.Inf  # for the moment. It's hardcoded in the fortran code.. weird
    nM = len(atoms) // 3
    a = atoms.cell.diagonal()
    a2 = a/2.
    molit = lambda L,N:[L[i*N:(i+1)*N] for i in range(len(L)//N)]
    rCM = np.array([mol.get_center_of_mass() for mol in molit(atoms, 3)]).T  # because fortran style 
    
    eT = np.zeros((3, nM))
    dEdr = np.zeros((3, 3, nM))

    for i in range(nM):
        for j in range(3):
            eT[j, i] = 0.
            for k in range(3):
                dEdr[k, j, i] = 0.0
        for j in range(nM):
            if j == i: # goto 11
                continue 
            for k in range(3):
                dr[k] = rCM[k, i] - rCM[k, j]
                if dr[k] > a2[k]:  # a MIC
                    dr[k] = dr[k] - a[k]
                elif dr[k] < -a2[k]:
                    dr[k] = dr[k] + a[k]

            r2 = dr[0]**2 + dr[1]**2 + dr[2]**2

            if r2 > rMax2:
                continue  # out of cutoff
            r1 = np.sqrt(r2)
            swFunc = SF(r1)  

            r3 = r1 * r2
            r5 = r3 * r2
            r7 = r5 * r2

            # Dipole Field
            u, eD, dEdr1 = dField(dr, r2, r3, r5, dpole, j) 
            uD += u # but u is just zero!! argh - not in my implementation.. but..
            for k in range(3):
                eT[k, i] += eD[k] * swFunc
                for l in range(3):
                    dEdr[k, l, i] += dEdr1[k, l] * swFunc

            # Quadrupole Field
            u, eq, dEdr1 = qField(dr, r2, r5, r7, qpole, j)
            uQ += u
            for k in range(3):
                eT[k, i] += eq[k] * swFunc
                for l in range(3):
                    dEdr[k, l, i] += dEdr1[k, l] * swFunc 
    #print uD, uQ
    return eT, dEdr

#----------------------------------------------------------------------+
#     Calculate derivatives of the electric field.                     |
#----------------------------------------------------------------------+
def calcDv(atoms, dpole, qpole, opole, hpole):
           #d1v, d2v, d3v, d4v, d5v, rMax2, fsf)

    nM = len(atoms) // 3
    d1v = np.zeros((3, nM))
    d2v = np.zeros((3, 3, nM))
    d3v = np.zeros((3, 3, 3, nM))
    d4v = np.zeros((3, 3, 3, 3, nM))
    d5v = np.zeros((3, 3, 3, 3, 3, nM))
    
    d1d = np.zeros(3)
    d2d = np.zeros((3, 3))
    d3d = np.zeros((3, 3, 3))
    d4d = np.zeros((3, 3, 3, 3))
    d5d = np.zeros((3, 3, 3, 3, 3))

    d1a = np.zeros_like(d1d)
    d2a = np.zeros_like(d2d)
    d3a = np.zeros_like(d3d)
    d4a = np.zeros_like(d4d)
    d5a = np.zeros_like(d5d)
    
    rMax2 = np.Inf  # for the moment. It's hardcoded in the fortran code.. weird
    a = atoms.cell.diagonal()
    a2 = a/2.
    molit = lambda L,N:[L[i*N:(i+1)*N] for i in range(len(L)//N)]
    rCM = np.array([mol.get_center_of_mass() for mol in molit(atoms, 3)]).T  # because fortran style 
    
    #rCM = np.array([[-1.3928565829865520,  1.5140099377225098],
    #                [-0.24966420410953841,-0.13887823022017820],
    #                [ 0.        ,  0.        ]])


    d = np.zeros(3)
    q = np.zeros((3, 3))
    o = np.zeros((3, 3, 3))
    h = np.zeros((3, 3, 3, 3))

    in2 = np.zeros(2, dtype=int)
    in3 = np.zeros(3, dtype=int)
    in4 = np.zeros(4, dtype=int)
    in5 = np.zeros(5, dtype=int)

    dr = np.zeros(3)
    
    for n in range(nM):
        for m in range(nM):
            if n == m:
                continue
            for i in range(3):
                dr[i] = rCM[i, n] - rCM[i, m]
                if dr[i] > a2[i]:
                    dr[i] = dr[i] - a[i]
                elif dr[i] < -a2[i]:
                    dr[i] = dr[i] + a[i]
                    
                #dr[i] = dr[i] + re[i]

            r2 = dr[0]**2 + dr[1]**2 + dr[2]**2 
            if r2 > rMax2:
                continue
            r1 = np.sqrt(r2)
            swFunc, dSdr = SFdsf(r1)
                     

            for i in range(3):
                d[i] = dpole[i, m]
                     
            # print 'd into dDpole: {0:s}, dr: {1:s}'.format(d, dr) XXX CHECKED
            d1a, d2a, d3a, d4a, d5a = dDpole(d, dr)
            #print 'd1a: {0:s}'.format(d1a)    XXX CHECKED

            for j in range(3):
                for i in range(3):
                    q[i, j] = qpole[i, j, m]
            # print 'q into dDpole: {0:s}, dr: {1:s}'.format(q, dr) XXX CHECKED!
            d1d, d2d, d3d, d4d, d5d = dQpole(q, dr)
            # print 'd1d after dQpole: {0:s}'.format(d1d) XXX CHECKED
            #print 'd2d after dQpole: {0:s}'.format(d2d) 
            d1a, d2a, d3a, d4a, d5a = addDerivA(d1a, d2a, d3a, d4a, d5a, 
                                                d1d, d2d, d3d, d4d, d5d)
            # print 'd1a after addDerivA: {0:s}'.format(d1a) XXX checked
            #print 'mol {0},{1} d2a after addDerivA: {2:s}'.format(n, m, d2a)
            for k in range(3):
                for j in range(3):
                    for i in range(3):
                        o[i, j, k] = opole[i, j, k, m]
            d1d, d2d, d3d, d4d, d5d = dOpole(o, dr)
            d1a, d2a, d3a, d4a, d5a = addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d,d3d, d4d, d5d)
            
            for l in range(3):
                for k in range(3):
                    for j in range(3):
                        for i in range(3):
                            h[i, j, k, l] = hpole[i, j, k, l, m]
            d1d, d2d, d3d, d4d, d5d = dHpole(h, dr)
            d1a, d2a, d3a, d4a, d5a = addDerivA(d1a, d2a, d3a, d4a, d5a, d1d, d2d, d3d, d4d, d5d)
            d1v, d2v, d3v, d4v, d5v = addDeriv(d1v, d2v, d3v, d4v, d5v, d1a, d2a, d3a, d4a, d5a,
                     n, swFunc)
            
            fsf = addSwitchingForce(d1a, d2a, d3a, d4a, n,dSdr, dr, r1, 
                                    dpole, qpole, opole, hpole)

    #Copy all the permutations. (Is this really necessary??)
    for i in range(3):
        for j in range(3):
            in2[0] = i
            in2[1] = j
            insertIN(in2, 1)
            
            for n in range(nM):
                d2v[i, j, n] = d2v[in2[0], in2[1], n]
            
            for k in range(3):
                for ii in range(2):
                    in3[ii] = in2[ii]
                                   
                in3[2] = k
                insertIN(in3, 2)

                for n in range(nM):
                    d3v[i, j, k, n] = d3v[in3[0], in3[1], in3[2], n]

                for l in range(3):
                    for ii in range(3):
                         in4[ii] = in3[ii]
                    in4[3] = l  
                    insertIN(in4, 3)

                    for n in range(nM):
                        d4v[i, j, k, l, n] = d4v[in4[0], in4[1], in4[2], in4[3], n]
                     
                    for m in range(3):
                        for ii in range(4):
                            in5[ii] = in4[ii]
                        in5[4] = m
                        insertIN(in5, 4)
                     
                        for n in range(nM):
                            d5v[i, j, k, l, m, n] = d5v[in5[0], in5[1], in5[2], in5[3],in5[4], n]
                                   
    return d1v, d2v, d3v, d4v, d5v


def calcEnergy(dpole, qpole, opole, hpole, d1v, d2v, d3v, d4v, nM):
    
    uTot = 0.
    ud = 0.
    uq = 0.
    uo = 0.
    uh = 0.

    for n in range(nM):
        for i in range(3):
            # Energy of the dipole
            du = d1v[i , n] * dpole[i, n]
            uTot += du
            ud += du
            for j in range(3):
                #   Energy of the quadrupole
                du = d2v[j, i, n] * qpole[j, i, n] / 3.
                uTot += du
                uq += du 
                for k in range(3):
                    #   Energy of the octopole
                    du = d3v[k, j, i, n] * opole[k, j, i, n] / 15.
                    uTot += du
                    uo += du
                    for l in range(3):
                        # Energy of the hexadecapole
                        du = d4v[l, k, j, i, n] * hpole[l, k, j, i, n] / 105.
                        uTot += du
                        uh += du

    uTot = uTot / 2.
    ud = ud / 2.
    uq = uq / 2.
    uo = uo / 2.
    uh = uh / 2.

    return uTot, ud, uq, uo, uh

